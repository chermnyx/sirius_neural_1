import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
os.environ['CUDA_VISIBLE_DEVICES'] = '2,3'

import numpy as np

from pathlib import Path

import keras
from keras.models import Model
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense, Conv2D, MaxPooling2D, Activation
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras import backend as K
K.set_image_data_format('channels_last')

img_rows, img_cols = 128, 128
if K.image_data_format() == "channels_first":
    input_shape = (3, img_rows, img_cols)
else:
    input_shape = (img_rows, img_cols, 3)

from PIL import ImageFile

from tensorflow.python.client import device_lib

ImageFile.LOAD_TRUNCATED_IMAGES = True

print(device_lib.list_local_devices())

p = '.'

train_data_dir = './data_bin/train'
validation_data_dir = './data_bin/val'
nb_train_samples = 3248
nb_validation_samples = 800
nb_classes = 2
epochs = 100
batch_size = 8


model = applications.VGG16(
    weights='imagenet',
     include_top=False,
     input_shape=input_shape)
print('Model loaded.')

# # build a classifier model to put on top of the convolutional model
top_model = Sequential()
top_model.add(Flatten(input_shape=model.output_shape[1:]))
top_model.add(Dense(128, activation='relu'))
top_model.add(Dropout(0.5))
top_model.add(Dense(2, activation='softmax'))

# note that it is necessary to start with a fully-trained
# classifier, including the top classifier,
# in order to successfully do fine-tuning
# top_model.load_weights(top_model_weights_path)

# add the model on top of the convolutional base
#model.add(top_model)
model = Model(inputs=model.inputs, outputs=top_model(model.outputs))
for layer in model.layers[:19]:
    layer.trainable = False

# top_model = Sequential()

# top_model.add(Flatten(input_shape=input_shape))
# top_model.add(Dense(64))
# top_model.add(Dense(128))
# top_model.add(Dense(2, activation='sigmoid'))

print(model.summary())

model.compile(
    loss='binary_crossentropy',
    optimizer=optimizers.SGD(lr=0.01),
    metrics=['accuracy'])

checkpoint = ModelCheckpoint(
    'bests_weight.h5', monitor="val_acc", verbose=1, save_best_only=True)

reduce_lr = ReduceLROnPlateau(
    monitor='val_acc', factor=0.2, patience=2, min_lr=0.000001, verbose=1)

stop = EarlyStopping(monitor='val_acc', patience=5, verbose=1)

callbacks_list = [checkpoint, reduce_lr, stop]

# prepare data augmentation configuration
train_datagen = ImageDataGenerator(
    # rescale=1. / 255,
    # rotation_range=360,
    # width_shift_range=0.2,
    # height_shift_range=0.2,
    # shear_range=0.2,
    # zoom_rangetrain_datage=0.2,
    horizontal_flip=True,
    vertical_flip=True
    )

test_datagen = ImageDataGenerator()

train_generator = train_datagen.flow_from_directory(
    train_data_dir, target_size=(img_rows, img_cols), batch_size=batch_size)

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_rows, img_cols),
    batch_size=batch_size)

# fine-tune the model
model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples,
    callbacks=callbacks_list)

scores = model.evaluate_generator(validation_generator, )
print("%s: %.2f%% with" % (top_model.metrics_names[1], scores[1] * 100))
